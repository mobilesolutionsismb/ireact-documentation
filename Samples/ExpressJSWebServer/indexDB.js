const express = require('express');
const os = require("os");

const app = express();

app.listen(5432, () => {
        console.log('DB is live!');

        app.get('/', (req, res) => {
                console.log(new Date(), 'received request');
                console.log(os.hostname());
                return res.send('<h1>DB</h1><br><br>' + JSON.stringify(os.hostname()) +"<br><br>"+ JSON.stringify(os.networkInterfaces()));
        });
});