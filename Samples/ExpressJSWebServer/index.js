const express = require('express');
const os = require("os");
const axios = require("axios");

const app = express();

app.listen(8080, () => {
  console.log('we are live!');
  let DB_HOST = "10.0.0.5";
  let DB_PORT = 5432;
  app.get('/', (req, res) => {
    console.log(new Date(), 'received request');
    let url = 'http://' + DB_HOST + ":" + DB_PORT;
    console.log('URL', url);
    axios.get(url)
      .then(function(response) {
        console.log(response);
        return res.send('<h1>DB RESPONSE</h1><br><br><div style="background-color:cyan">' + response.data + "</div><br><br>" + JSON.stringify(os.hostname()) + "<br><br>" + JSON.stringify(os.networkInterfaces()));
      })
      .catch(function(error) {
        console.log(error);
        return res.send('<h1>ERROR CONTACTING DB</h1><br><br><div style="background-color:cyan">' + JSON.stringify(error) + "</div><br><br>" + JSON.stringify(os.hostname()) + "<br><br>" + JSON.stringify(os.networkInterfaces()));
      });   
  });
});