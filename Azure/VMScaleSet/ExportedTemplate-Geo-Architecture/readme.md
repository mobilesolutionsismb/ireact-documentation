The scale sets have a "VirtualMachineProfile" that is an image for a general machine.
Each new machine in the scale set starts from this profile.

The profile I set says to each new machine to download a script from a storage account and execute this to setup all the stuff needed.

The scale sets are in the same virtual network and only the geoserver's one is accessible from outside.

Now I added 2 example script (one for each scale set) that start 2 webservers, only to test the load balancing. The user calls the first one (that will be the geoserver) and then it calls the second one (that will be the db) and return the concatenation of the responses to the user.

If you refresh sometimes you can see that the load balancers work (except for the db scale set because the load balancer maintains the session).

Some tips:
- To "open" a new port you need to go to the load balancer (* dblb * for database and * geoserverlb * for geoserver) settings and add a new "Load balancing rules"
- Before adding a new rule on the load balancer you need to set a probe on that port
- The ports used in my example for communication are:
	- outside: 80 -> inside: 8080 for the geoserver
	- outside: 5432 -> inside: 5432 for the db 

Now you "only" need to 
- write the script that, starting from an empty ubuntu 16.04 machine, set up the geoserver
- write another script that, starting from an empty ubuntu 16.04 machine, set up the PostegreSQL

The script will be uploaded to the blob storage account * ireactgeo *, you can to that in the Azure portal:
- open storage account
- Blobs
- click on container name 
- click upload

After upload a new script you need to restart all already running machine following these steps:
- open the scale set
- Instances
- Select all the instances
- Reimage (or delete and then Scaling -> set the number of instances you prefer)

Thoubleshooting:
- Initialization script:
	- connect to the machine via ssh (using load balancer ip and NAT frontend port that you find in the load balancer nat rules)
	  remember that db's vms an be accessed only in the same vnet so you need to connect to a geoserver's vm and then via ssh to the db's vm 
	- as superuser open /var/lib/waagent/custom-script/download/<the biggest number>/stderr to see your script error log
	- as superuser open /var/lib/waagent/custom-script/download/<the biggest number>/stdout to see your script output