# Script to remove a nat pool to an Azure VM Scale Set

# Login
Login-AzureRmAccount

# Select the right subscription
Select-AzureRmSubscription -SubscriptionName "BizSpark Plus"

# SCALE SET CONFIG
$resourceGroupName = "IREACT-Geo-ScaleSet"
$scaleSetName = "ireactgeo"
$loadBalancerName = "ireactgeolb"

# NEW NAT RULE CONFIG
$inboundNatPoolName = "newSSHpool3"
$frontendPoolRangeStart = 64000
$frontendPoolRangeEnd = 64100
$backendVMPort = 2222

# GET Load Balancer NAT pool
$slb = Get-AzureRmLoadBalancer -Name $loadBalancerName -ResourceGroupName $resourceGroupName
$oldNatPoolConfig = $slb | Get-AzureRmLoadBalancerInboundNatPoolConfig -Name $inboundNatPoolName

# Update Scale Set model deleting NAT pool config
$vmss = Get-AzureRmVmss -ResourceGroupName $resourceGroupName -VMScaleSetName $scaleSetName
$ipcfg = $vmss.VirtualMachineProfile.NetworkProfile.NetworkInterfaceConfigurations[0].IpConfigurations[0]

$b = $ipcfg.LoadBalancerInboundNatPools
$oldNatIndex = (0..($b.Count-1)) | where {$b[$_].id -eq $oldNatPoolConfig.id}
$b.RemoveAt($oldNatIndex)

$vmss = $vmss | Update-AzureRmVmss -ResourceGroupName $resourceGroupName -Name $scaleSetName

# Update each VM instance in the scale set to fit to the new model
$vmssInstances = Get-AzureRmVmssVM -ResourceGroupName $resourceGroupName  -VMScaleSetName $scaleSetName 
Update-AzureRmVmssInstance -ResourceGroupName $resourceGroupName -VMScaleSetName $scaleSetName -InstanceId $vmssInstances.InstanceId

# Update Load Balancer deleting the Nat Pool
$slb = Get-AzureRmLoadBalancer -Name $loadBalancerName -ResourceGroupName $resourceGroupName
$slb = $slb | Remove-AzureRmLoadBalancerInboundNatPoolConfig -Name $inboundNatPoolName
$slb = $slb | Set-AzureRmLoadBalancer