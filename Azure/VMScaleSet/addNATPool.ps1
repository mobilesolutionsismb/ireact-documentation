# Script to add a nat pool to an Azure VM Scale Set

# Login
Login-AzureRmAccount

# Select the right subscription
Select-AzureRmSubscription -SubscriptionName "BizSpark Plus"

# SCALE SET CONFIG
$resourceGroupName = "IREACT-Geo-ScaleSet"
$scaleSetName = "ireactgeo"
$loadBalancerName = "ireactgeolb"

# NEW NAT RULE CONFIG
$inboundNatPoolName = "newSSHpool3"
$frontendPoolRangeStart = 64000
$frontendPoolRangeEnd = 64100
$backendVMPort = 2222

# Update Load Balancer creating a new NAT pool
$slb = Get-AzureRmLoadBalancer -Name $loadBalancerName -ResourceGroupName $resourceGroupName
$slb = $slb | Add-AzureRmLoadBalancerInboundNatPoolConfig -Name $inboundNatPoolName -FrontendIpConfigurationId $slb.FrontendIpConfigurations[0].id -Protocol "Tcp" -FrontendPortRangeStart $frontendPoolRangeStart -FrontendPortRangeEnd $frontendPoolRangeEnd -BackendPort $backendVMPort
$slb = $slb | Set-AzureRmLoadBalancerInboundNatPoolConfig -Name $inboundNatPoolName -FrontendIpConfigurationId $slb.FrontendIpConfigurations[0].id -Protocol "Tcp" -FrontendPortRangeStart $frontendPoolRangeStart -FrontendPortRangeEnd $frontendPoolRangeEnd -BackendPort $backendVMPort
$slb = $slb | Set-AzureRmLoadBalancer

# Update Scale Set model adding new NAT pool config
$vmss = Get-AzureRmVmss -ResourceGroupName $resourceGroupName -VMScaleSetName $scaleSetName
$ipcfg = $vmss.VirtualMachineProfile.NetworkProfile.NetworkInterfaceConfigurations[0].IpConfigurations[0]
foreach ($id in $slb.InboundNatPools.id) {
    if (! $ipcfg.LoadBalancerInboundNatPools.id.Contains($id)) {
        $ipcfg.LoadBalancerInboundNatPools.Add($id)
    }
}
$vmss = $vmss | Update-AzureRmVmss -ResourceGroupName $resourceGroupName -Name $scaleSetName

# Update each VM instance in the scale set to fit to the new model
$vmssInstances = Get-AzureRmVmssVM -ResourceGroupName $resourceGroupName  -VMScaleSetName $scaleSetName 
Update-AzureRmVmssInstance -ResourceGroupName $resourceGroupName -VMScaleSetName $scaleSetName -InstanceId $vmssInstances.InstanceId