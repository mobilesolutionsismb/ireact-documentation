# Login
Login-AzureRmAccount

# Select the right subscription
Select-AzureRmSubscription -SubscriptionName "BizSpark Plus"

# SCALE SET CONFIG
$resourceGroupName = "IREACT-GEO-ARCHITECTURE-V3"
$scaleSetName = "postgresql"

# get VMSS
$vmss = Get-AzureRmVmss -ResourceGroupName $resourceGroupName -VMScaleSetName $scaleSetName

# add premium datadisk without cache
$vmss = Add-AzureRmVmssDataDisk -VirtualMachineScaleSet $vmss -Lun 1 -Caching 'None' -CreateOption Empty -DiskSizeGB 1023 -StorageAccountType PremiumLRS

Update-AzureRmVmss -ResourceGroupName $resourceGroupName -Name $scaleSetName -VirtualMachineScaleSet $vmss