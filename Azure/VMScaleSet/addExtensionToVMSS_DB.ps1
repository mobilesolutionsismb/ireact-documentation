# Login
Login-AzureRmAccount

# Select the right subscription
Select-AzureRmSubscription -SubscriptionName "BizSpark Plus"

# SCALE SET CONFIG
$resourceGroupName = "ireact-geo-architecture-v2"
$scaleSetName = "geoserver"

# get VMSS
$vmss = Get-AzureRmVmss -ResourceGroupName $resourceGroupName -VMScaleSetName $scaleSetName

# get second VMSS to copy the extension config
$vmss2 = Get-AzureRmVmss -ResourceGroupName "todelete2" -VMScaleSetName "testcs"
$extp = $vmss2.VirtualMachineProfile.ExtensionProfile
$vmss.VirtualMachineProfile.ExtensionProfile = $extp

# new extension Settings
$jo = New-Object -TypeName Newtonsoft.Json.Linq.JObject
$jval = New-Object -TypeName Newtonsoft.Json.Linq.JValue -ArgumentList "https://teststoragegeoarch.blob.core.windows.net/initvmss/initDB.sh"
$jo.Add("fileUris", $jval)
$jval = New-Object -TypeName Newtonsoft.Json.Linq.JValue -ArgumentList "./initDB.sh"
$jo.Add("commandToExecute", $jval)

# remove disk settings because we use managed disks and we don't need to update disks settings
$vmss.VirtualMachineProfile.ExtensionProfile.Extensions[0].Settings = $jo
$vmss.VirtualMachineProfile.StorageProfile.OsDisk = $null

# update vmss template
$vmss = $vmss | Update-AzureRmVmss -ResourceGroupName $resourceGroupName -Name $scaleSetName


# Update each VM instance in the scale set to fit to the new model
$vmssInstances = Get-AzureRmVmssVM -ResourceGroupName $resourceGroupName  -VMScaleSetName $scaleSetName 
Update-AzureRmVmssInstance -ResourceGroupName $resourceGroupName -VMScaleSetName $scaleSetName -InstanceId $vmssInstances.InstanceId
