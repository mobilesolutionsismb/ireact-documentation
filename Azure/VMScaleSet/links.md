[DASHBOARD](https://github.com/gbowerman/vmssdashboard)

[HOW-TO DEPLOY](https://docs.microsoft.com/en-us/azure/virtual-machine-scale-sets/virtual-machine-scale-sets-deploy-app)

[EXTENSIONS FOR WINDOWS](https://docs.microsoft.com/en-us/azure/virtual-machines/virtual-machines-windows-extensions-features?toc=%2fazure%2fvirtual-machines%2fwindows%2ftoc.json)

[EXTENSIONS FOR LINUX](https://github.com/Microsoft/azure-docs/blob/master/articles/virtual-machines/virtual-machines-linux-extensions-customscript.md)

[UPGRADE VM INSTANCES](https://msftstack.wordpress.com/2016/05/17/how-to-upgrade-an-azure-vm-scale-set-without-shutting-it-down/)

[UPGRADE VMSS PROPS](https://msftstack.wordpress.com/2016/11/15/azure-scale-set-upgrade-policy-explained/)

[OVERVIEW MANAGEMENT WITH POWERSHELL](https://russellyoung.net/2016/09/01/managing-vm-scale-sets-vmss-with-powershell-and-arm-templates/)

[GEOSERVER ARCHITECTURE](https://goo.gl/flFDnS)

Architecture image similar architecture but without using scale sets:

![Architecture image similar architecture but without using scale sets](./GeoserverArchitecture.png)