# Login
Login-AzureRmAccount

# Select the right subscription
Select-AzureRmSubscription -SubscriptionName "BizSpark Plus"

# SCALE SET CONFIG
$resourceGroupName = "Test-Geo-Architecture"
# $scaleSetName = "testgeoarchitecture"

$scaleSetName = "testdbvms"

# get VMSS
$vmss = Get-AzureRmVmss -ResourceGroupName $resourceGroupName -VMScaleSetName $scaleSetName

# get second VMSS to copy the extension config
$vmss2 = Get-AzureRmVmss -ResourceGroupName "todelete2" -VMScaleSetName "testcs"
$extp = $vmss2.VirtualMachineProfile.ExtensionProfile
$vmss.VirtualMachineProfile.ExtensionProfile = $extp

# new extension Settings
$jo = New-Object -TypeName Newtonsoft.Json.Linq.JObject
# $jval = New-Object -TypeName Newtonsoft.Json.Linq.JArray @('https://teststoragegeoarch.blob.core.windows.net/initvmss/initWeb.sh')
$jval = New-Object -TypeName Newtonsoft.Json.Linq.JArray @('https://teststoragegeoarch.blob.core.windows.net/initvmss/initDB.sh')
$jo.Add("fileUris", $jval)
# $jval = New-Object -TypeName Newtonsoft.Json.Linq.JValue -ArgumentList "/bin/bash ./initWeb.sh"
$jval = New-Object -TypeName Newtonsoft.Json.Linq.JValue -ArgumentList "/bin/bash ./initDB.sh"
$jo.Add("commandToExecute", $jval)
$vmss.VirtualMachineProfile.ExtensionProfile.Extensions[0].Settings = $jo

$vmss.VirtualMachineProfile.ExtensionProfile.Extensions[0].Name = "InitScript"
$vmss.VirtualMachineProfile.ExtensionProfile.Extensions[0].Publisher = "Microsoft.Azure.Extensions"
$vmss.VirtualMachineProfile.ExtensionProfile.Extensions[0].Type = "CustomScript"
$vmss.VirtualMachineProfile.ExtensionProfile.Extensions[0].TypeHandlerVersion = "2.0"

# remove disk settings because we use managed disks and we don't need to update disks settings
$vmss.VirtualMachineProfile.StorageProfile.OsDisk = $null

# update vmss template
$vmss = $vmss | Update-AzureRmVmss -ResourceGroupName $resourceGroupName -Name $scaleSetName


# Update each VM instance in the scale set to fit to the new model
$vmssInstances = Get-AzureRmVmssVM -ResourceGroupName $resourceGroupName  -VMScaleSetName $scaleSetName 
Update-AzureRmVmssInstance -ResourceGroupName $resourceGroupName -VMScaleSetName $scaleSetName -InstanceId $vmssInstances.InstanceId

